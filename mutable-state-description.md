<!-- Approved licence: YES -->
<!-- No Proprietary Software: YES -->
<!-- How long has your team been accepting publicly submitted contributions? 
  More than 2 years
-->
<!-- How many regular contributors does your team have?
  11-20 people 
-->

## Project short title

Reducing global mutable state in the OCaml compiler codebase.

## Long description

The OCaml compiler is itself written in OCaml, a functional
programming language. The compiler codebase generally favors
a functional programming style, which avoids mutable state and in
general side-effects that are visible at a global level, and may
impact other parts of the compiler.

However, some parts of the compiler do use mutable state for
convenience; either locally (a given function uses some mutable state
for its internal processing, that is thrown away when the
function returns) or more globally (a module contains a piece of
mutable state that needs to be initialized before certain functions
can run, and cleaned up before using them again.)

Global mutable state is arguably a code smell: it makes understanding
the code more difficult, it creates opportunities for bugs (if it is
not initialized / cleaned-up properly), and it makes it harder to use
the compiler internals as a library from user programs, because users
now have to handle the compiler state correctly.

(Note: most compilers use various forms of global mutable state,
sometimes for essential reasons. Even those implemented in functional
languages! GHC for example, the main compiler for Haskell, itself
implemented in Haskell, has large parts of its codebase in a so-called
"IO monad" that gives access to global mutable state.)

The purpose of this project is to identify parts of the compiler
codebase that use mutable states in ways that could be improved by
either making the mutable state more local / explicit (having an
explicit "state" parameter that is passed around in the functions that
need it), or in some cases getting rid of mutable state completely
(when it is not essential for programming). We expect that some such
changes will be relatively easy to do, and that some others may be
more difficult, or even too difficult (for mutable state that evolves
in subtle ways and is used across several modules); we propose to
focus on the easier parts first!

# Minimum system requirements

There are no specific computer requirements for this project. The
OCaml compiler implementation builds in a few minutes on most laptops,
and can be built under most operating systems (Windows, OSX, Linux,
BSD, etc.).

# How can applicants make a contribution to your project? 

<!-- included from newcomer-jobs-2020.html -->

# Repository

<https://github.com/ocaml/ocaml>

# Issue tracker

<https://github.com/ocaml/ocaml/issues>

# Newcomer issue tag

newcomer-job

# Intern tasks

The general task of the intern would be to identify parts of the
compiler that use mutable state in a way that could be made more local
or avoided, and propose a refactoring changes to fix the issue. The
intern would get feedback (in particular code review) from the mentor,
and then be encouraged to submit the change to the upstream compiler
implementation.

The refactoring changes will likely require understanding the affected
code, and turning this understanding into documentation should be
considered another goal of this project.

It may not be easy at first to understand which uses of mutable state
can be improved in this way and which cannot, which refactoring would
be relatively easy or could turn out to be very hard. Many parts of the
compiler rely on complex and under-documented invariants, so even expert
compiler maintainers get this wrong on a regular basis. We thus expect
that some attempted changes will not work out; the intern should
communicate frequently with the mentor, who may be able to get an
early sense of whether a change in a given area is within reach or too
ambitious.

Note that even if a change proposal is correct, it may be rejected by
compiler maintainers if it is perceived to be too heavy/invasive, or
to make the code less maintenable. Having some change proposals turned
down is a natural part of contributing to an open source project; it is
not always pleasant, but also a useful learning experience. (Again,
regular communication with the mentor should ensure that this only
happens rarely.)


# Intern benefits

This work of refactoring is a way to learn a bit about many different
parts of a production compiler.

It is also an opportunity to reflect about programming style, and the
compromises that we often make between general recommendations and
pragmatic choices for convenience.


# Community benefits

The users of the OCaml programming language all benefit from
refactorings that make their compiler easier to maintain and improve
in the future, and also make the compiler services easier to use as
a library in their own programs.

<!-- Unapproved license description:

None (the Outreachy people reviewed our CLA process in the past
-->

<!-- Unapproved license description:

None (the Outreachy people reviewed our CLA process in the past
-->

<!-- Proprietary software description:
None
-->

<!-- Accepting New Contributors?
Yes: My project is open to new contributors -->
