<!-- Approved licence: YES -->
<!-- No Proprietary Software: YES -->
<!-- How long has your team been accepting publicly submitted contributions?
  More than 2 years
-->
<!-- How many regular contributors does your team have?
  11-20 people
-->

## Project short title

Structured output format for the OCaml compiler messages

## Long description

As semi-interactive programs, the OCaml compilers and REPL emits a lot of
messages: the compilers display warnings or error messages, it
may output some of its internal representation when requested.
The interactive REPL does the same but also has to print the
result of each evaluated phrase during the run of an interactive
session.

Traditionally, all of this outputs have been destined for human
consumption. However, human readable formats are not ideal for
other tools. Typically, a build system running the OCaml
compiler might want to intercept error messages during the
build, and display at a time of its own convenience. Currently,
this can be done by parsing the output of the OCaml compiler,
but this is awkward and fragile. Another option is to use the
compiler libraries; it gives more control, but keeping up to
date with the various version of the compiler library is often
a lot of work.

A better solution would be to add an option to the OCaml
compiler to switch all of its output to a structured format,
such as JSON. These structured format have the advantages of
being both human and machine readable. Moreover, they are
a common ground used by lot of softwares for data exchange. Thus
switching to structured output format should make it much easier
for other tools, or data-processing scripts, to process OCaml
messages.


# Minimum system requirements

There are no specific computer requirements for this project. The
OCaml compiler implementation builds in a few minutes on most laptops,
and can be built under most operating systems (Windows, OSX, Linux,
BSD, etc.).

# How can applicants make a contribution to your project?

<!-- included from newcomer-jobs-2020.html -->

# Repository

<https://github.com/ocaml/ocaml>

# Issue tracker

<https://caml.inria.fr/mantis/>

# Newcomer issue tag

newcomer-job, newcomer-job-advanced

# Intern tasks

The first part of the project would be to add a JSON output to ocamldep.
Ocamldep is a self-contained program that can be converted in a non-invasive way.

The second task will see the intern move to the main compiler, focusing on warnings first.
Warning messages are also quite well separated from the rest of the compiler,
and a structured output format should be implementable independently.

In the third task, with more concrete use cases at hand, we shall take the time
to design a small combinator library for generating JSON output easily.

In the fourth task, we arrive to the core of the internship: converting the error
message themselves to a JSON output.

Finally, if there is some time remaining, we would shall design a versioning API
for our structured output.

# Intern benefits

Refactoring the warning and error message in the OCaml compiler is a nice and
gentle way to familiarize oneself with the compiler code base. It is an opportunity
to learn about compiler design and architecture.

There should be a lot of opportunity to refine the JSON output library,
offering a place to experiment and learn about combinator library.

Finally, at the outset of the internship, we may have the time to ponder
about the question of versioning, backward and future compatibility;
exploring a hard question that is present in both academia and industry.

# Community benefits

Having structured output in the compiler and the REPL shall ease integration
with higher level tooling like dune, merlin, or the "better-error-message"
of ReasonML.


<!-- Unapproved license description:

None (the Outreachy people reviewed our CLA process in the past
-->

<!-- Unapproved license description:

None (the Outreachy people reviewed our CLA process in the past
-->

<!-- Proprietary software description:
None
-->

<!-- Accepting New Contributors?
Yes: My project is open to new contributors -->
