<!-- Approved licence: YES -->
<!-- No Proprietary Software: YES -->
<!-- How long has your team been accepting publicly submitted contributions? 
  More than 2 years
-->
<!-- How many regular contributors does your team have?
  11-20 people 
-->

## Project short title

## Long description

<!--
# Minimum system requirements
  No specific system requirements.
-->


# How can applicants make a contribution to your project? 

<!-- included from newcomer-jobs-2020.html -->

# Repository

<https://github.com/ocaml/ocaml>

# Issue tracker

<https://github.com/ocaml/ocaml/issues>

# Newcomer issue tag

newcomer-job

# Intern tasks

TODO

# Intern benefits

TODO

# Community benefits

TODO

<!-- Unapproved license description:

None (the Outreachy people reviewed our CLA process in the past
-->

<!-- Unapproved license description:

None (the Outreachy people reviewed our CLA process in the past
-->

<!-- Proprietary software description:
None
-->

<!-- Accepting New Contributors?
Yes: My project is open to new contributors -->
